jest.mock('../src/service');

import { postShippingRoute } from '../src/controller';
import { mockFunction } from './util';
import httpMocks from 'node-mocks-http';
import { storeShipping } from '../src/service';

describe('Shipping Controller', () => {
  describe('Post Shipping Route', () => {
    test('When the body is empty, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/shipping',
        body: {},
      });
      const response = httpMocks.createResponse();

      await postShippingRoute(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the items is empty, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/shipping',
        body: { items: [], address: 'address' },
      });
      const response = httpMocks.createResponse();

      await postShippingRoute(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the items are missing _id, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/shipping',
        body: { items: [{ amount: 1 }], address: 'address' },
      });
      const response = httpMocks.createResponse();

      await postShippingRoute(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the items are missing amount, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/shipping',
        body: { items: [{ _id: '1' }], address: 'address' },
      });
      const response = httpMocks.createResponse();

      await postShippingRoute(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the items have negative amount, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/shipping',
        body: { items: [{ _id: '1', amount: -1 }], address: 'address' },
      });
      const response = httpMocks.createResponse();

      await postShippingRoute(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the items have string amount, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/shipping',
        body: { items: [{ _id: '1', amount: '1' }], address: 'address' },
      });
      const response = httpMocks.createResponse();

      await postShippingRoute(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the items have number _id, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/shipping',
        body: { items: [{ _id: 1, amount: 1 }], address: 'address' },
      });
      const response = httpMocks.createResponse();

      await postShippingRoute(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the items are valid, then the statusCode is 200', async () => {
      const mockedFn = mockFunction(storeShipping);
      mockedFn.mockImplementation(async () => {
        return { shipping: { _id: '1', items: [], address: 'address' } };
      });
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/shipping',
        body: { items: [{ _id: '1', amount: 1 }], address: 'address' },
      });
      const response = httpMocks.createResponse();

      await postShippingRoute(request, response);

      expect(response.statusCode).toEqual(200);
    });
  });
});
