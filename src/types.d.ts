export interface ItemData {
  _id: string;
  amount: number;
}

export interface ShippingData {
  _id: string;
  items: ItemData[];
  address: string;
}
