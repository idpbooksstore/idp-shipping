import { ItemData } from './types';
import { insert } from './model';

export const storeShipping = async (items: ItemData[], address: string) => insert(items, address);
