import { Shipping } from './schema';
import { ItemData, ShippingData } from './types';

interface Insert {
  shipping: ShippingData;
}

export const insert = async (items: ItemData[], address: string): Promise<Insert> => {
  const shipping = await Shipping.create({ items, address });
  return { shipping };
};
