import { storeShipping } from './service';
import Router, { Request, Response } from 'express';
import { validateShipping } from './validate';
import { ItemData } from './types';
export const router = Router();

interface PostShipping {
  items: ItemData[];
  address: string;
}

export const postShippingRoute = async (req: Request, res: Response) => {
  if (!validateShipping(req.body)) return res.status(400).send({ message: validateShipping.errors });
  try {
    const { items, address } = req.body as PostShipping;
    const { shipping } = await storeShipping(items, address);

    res.status(200).send({ message: 'Success', shipping });
  } catch (error) {
    return res.status(500).send({ message: error });
  }
};

router.post('/', postShippingRoute);
