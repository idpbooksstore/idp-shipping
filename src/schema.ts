import { Document, Schema, Model, model } from 'mongoose';
import { ShippingData } from './types';

export interface ShippingDocument extends Document, ShippingData {
  _id: string;
}

export interface ShippingModel extends Model<ShippingDocument> {}

const shippingSchema = new Schema<ShippingDocument, ShippingModel>({
  items: [{ _id: { type: String, required: true }, amount: { type: Number, required: true } }],
  address: { type: String, required: true },
});

export const Shipping = model<ShippingDocument, ShippingModel>('Shipping', shippingSchema);
