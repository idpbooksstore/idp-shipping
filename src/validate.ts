import Ajv from 'ajv';

const ajv = new Ajv();

ajv.addFormat('amount', {
  type: 'number',
  validate: (x) => x > 0 && isFinite(x),
});

ajv.addFormat('address', {
  type: 'string',
  validate: (x) => x.length > 0,
});

const stringSchema = {
  type: 'object',
  properties: {
    items: {
      type: 'array',
      items: {
        type: 'object',
        properties: { _id: { type: 'string' }, amount: { type: 'number', format: 'amount' } },
        required: ['_id', 'amount'],
        additionalProperties: false,
      },
      minItems: 1,
    },
    address: { type: 'string', format: 'address' },
  },
  required: ['items', 'address'],
  additionalProperties: false,
};

export const validateShipping = ajv.compile(stringSchema);
